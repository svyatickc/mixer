const left = document.querySelector("#left");
const right = document.querySelector("#right");
let tab;

chrome.tabs.query({active: true, currentWindow: true}, tabs => {
  if(chrome.runtime.lastError) {return;}
  tab = tabs[0];
  init();
})


updateState = () => {
  chrome.runtime.sendMessage({
    action: 'set_state', tabId: tab.id, tabUrl: tab.url, left: left.value || 100, right: right.value || 100
  })
}

init = () => {
  [left, right].forEach(node => {
    node.addEventListener("input", () => { console.log('update'); updateState() });
  });
  chrome.runtime.sendMessage({ action: 'get_state', tabId: tab.id}, response => {
    if(!response) {return;}
    left.value = response.left;
    left.dispatchEvent(new Event("input"));
    right.value = response.right;
    right.dispatchEvent(new Event("input"));
  })
}


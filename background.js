audioStates = {};
window.audioStates = audioStates;

const updateBadge = (tabId, text) => chrome.browserAction.setBadgeText({text, tabId});

const connectStream = (tabId, stream) => {
  const audioContext = new window.AudioContext;
  const source = audioContext.createMediaStreamSource(stream);
  const splitter = audioContext.createChannelSplitter(2);
  const merger = audioContext.createChannelMerger(2);
  const left = audioContext.createGain();
  const right = audioContext.createGain();

  splitter.connect(right, 1);
  right.connect(merger, 0, 1);

  source.connect(splitter);
  splitter.connect(left);
  left.connect(merger);
  merger.connect(audioContext.destination);

  audioStates[tabId] = {audioContext, right, left};
};

const setState = (tabId, left, right) => {
  audioStates[tabId].left.gain.value = left / 10;
  audioStates[tabId].right.gain.value = right / 10;
};

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if(message.action === 'set_state') {
    if(audioStates.hasOwnProperty(message.tabId)){
      setState(message.tabId, message.left, message.right);
      updateBadge(message.tabId, `${message.left}|${message.right}`)
    } else {
      chrome.tabCapture.capture({audio: true, video: false}, stream => {
        let err = chrome.runtime.lastError;
        if(err) {return;}

        connectStream(message.tabId, stream);
        setState(message.tabId, message.left, message.right);
        updateBadge(message.tabId, `${message.left}|${message.right}`)
      });
    }
  }

  if(message.action === 'get_state') {
    state = audioStates[message.tabId];
    if(!state) {return;}

    sendResponse({
      left: state.left.gain.value * 10,
      right: state.right.gain.value * 10,
    });
  }
})
